using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBouncer : MonoBehaviour
{

    public Camera myCam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = myCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
            print("Mousepos: " + mousePos);
            transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
        }
    }
}
