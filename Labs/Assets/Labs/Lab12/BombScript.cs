using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour
{
    SpriteRenderer spr;
    public Sprite explosion;

    float timer;
    int phase;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        phase = 0;
        timer = Time.realtimeSinceStartup;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.realtimeSinceStartup - timer > 5 && phase == 0)
        {
            phase = 1;
            spr.sprite = explosion;
        }
        else if(Time.realtimeSinceStartup - timer > 5.7 && phase == 1)
        {
            spr.enabled = false;
            phase = 2;
        }
    }
}
