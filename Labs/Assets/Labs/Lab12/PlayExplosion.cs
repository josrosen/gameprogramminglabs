using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayExplosion : MonoBehaviour
{
    float timer;
    bool soundPlayed;
    AudioSource sound;
    

    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<AudioSource>();
        timer = Time.realtimeSinceStartup;
        soundPlayed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup - timer > 5 && !soundPlayed)
        {
            sound.PlayOneShot(sound.clip);
            soundPlayed = true;
        }
    }
}
