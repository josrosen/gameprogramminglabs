using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTRandomWalk : BTNode
{
   protected Vector3 NextDestination { get; set; }
   public float speed = 4;

    public BTRandomWalk(BehaviorTree t) : base(t)
    {
        FindNextDestination();
    }

    public bool FindNextDestination()
    {
        //an object to write to that trygetvalue will use
        object o;

        //start by assuming we didn't find it
        bool found = false;
        found = Tree.Blackboard.TryGetValue("WorldBounds", out o);

        if (found)
        {
            Rect bounds = (Rect)o;
            float x = UnityEngine.Random.value * bounds.width;
            float y = UnityEngine.Random.value * bounds.height;

            NextDestination = new Vector3(x, y, NextDestination.z);
        }

        return found;
    }

    public override Result Execute()
    {
        Debug.Log("BTRandomWalk: destination is " + NextDestination);
        //have we made it to our destination
        if(Tree.gameObject.transform.position == NextDestination)
        {
            //fail if we can't find a next destination
          //  if (!FindNextDestination())
               // return Result.Failure;

            //else
                //not sure if the prof likes this
                return Result.Success;
        }
        else
        {
            //Otherwise, move toward our next destination at the given speed
            Tree.gameObject.transform.position = Vector3.MoveTowards(Tree.gameObject.transform.position, NextDestination, speed * Time.deltaTime);

            return Result.Running;
        }


    }


}
