using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTFail : BTNode
{
   public BTFail(BehaviorTree t): base(t)
    {

    }

    public override Result Execute()
    {
        return Result.Failure;
    }
}
