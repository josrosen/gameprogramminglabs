using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTRepeater : Decorator
{
    public BTRepeater(BehaviorTree t,  BTNode c) : base(t,c)
    {

    }

    public override Result Execute()
    {
        //execute the child first
        Result r = Child.Execute();

        Debug.Log("Child returned: " + r);
        
        //Always return running
        return Result.Running;
    }
}
