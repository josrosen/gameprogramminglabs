using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTSelectorNode : BTComposite
{
    private int currentNode = 0;
    private int failCount = 0;

    public BTSelectorNode(BehaviorTree t, BTNode[] children) : base(t, children)
    {

    }

    public override Result Execute()
    {

        if (currentNode < Children.Count)
        {
            //capture the result of the child
            Result result = Children[currentNode].Execute();

            //let the child run until it's finished
            if (result == Result.Running)
                return result;
            else
            {
                if (result == Result.Failure)
                {
                    failCount += 1;
                    currentNode++;
                    return Result.Running;
                }
                else
                {
                    return Result.Success;
                }
            }
        }

        if (failCount == Children.Count)
            return Result.Failure;
        else
            return Result.Success;
    }
}
