using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTRepeatUntilFailureNode : Decorator
{
    public BTRepeatUntilFailureNode(BehaviorTree t, BTNode c) : base(t,c)
    {

    }
    public override Result Execute()
    {
        //execute the child first
        Result r = Child.Execute();

        Debug.Log("Child returned: " + r);

        //Return running until fail
        if (r == Result.Failure)
            return Result.Failure;
        else
            return Result.Running;
    }
}
