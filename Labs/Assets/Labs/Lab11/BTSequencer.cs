using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTSequencer : BTComposite
{
    private int currentNode = 0;

    public BTSequencer(BehaviorTree t, BTNode[] children) : base(t, children)
    {

    }

    public override Result Execute()
    {
        Debug.Log("BTSequencer: number of children is " + Children.Count);

        if(currentNode < Children.Count)
        {
            
            //capture the result of the child
            Result result = Children[currentNode].Execute();

            //let the child run until it's finished
            if (result == Result.Running)
            {
                Debug.Log("BTSequencer: running node " + currentNode);
                return result; 
            }
            else if (result == Result.Failure)
            {
                Debug.Log("BTSequencer: failure on node " + currentNode);
                currentNode = 0;
                return result;
            }
            else
            {
                Debug.Log("BTSequencer: incrementing to node " + (currentNode + 1));
                currentNode++;
                //I don't know how the current computation will complete
                //so just return running
                if (currentNode < Children.Count)
                    return Result.Running;
                else
                {
                    currentNode = 0;
                    return Result.Success;
                }
            }
        }

        return Result.Success;
    }
}
