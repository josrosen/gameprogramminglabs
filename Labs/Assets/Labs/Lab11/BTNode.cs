using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTNode
{
    public enum Result { Running, Failure, Success }; 

    public BehaviorTree Tree { get; set; }

    public BTNode(BehaviorTree t)
    {
        //assign tree to be the t we passed in
        Tree = t;
    }

    //called by the Behavior Tree execution
    public virtual Result Execute()
    {
        return Result.Failure;
    }
}
