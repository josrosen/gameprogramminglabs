using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorTree : MonoBehaviour
{
    public int routine;
    //the root
    private BTNode mRoot;

    //has the behavior started
    private bool startedBehavior;

    //access to the behavior coroutine
    private Coroutine behavior;

    //access to the blackboard
    public Dictionary<string, object> Blackboard { get; set; }

    //public access to the root
    public BTNode Root { get { return mRoot; } }

    

    void Start()
    {
        //Create the blackboard
        Blackboard = new Dictionary<string, object>();
        //Setup keys that every AI needs
        Blackboard.Add("WorldBounds", new Rect(0, 0, 5, 5));

        startedBehavior = false;

        if (routine == 1)
        {
            mRoot = new BTRepeatUntilFailureNode(this,
                        new BTSequencer(this,
                            new BTNode[] { new BTRandomWalk(this), new BTRandomWalk(this), new BTFail(this), new BTRandomWalk(this), new BTRandomWalk(this) }));
        }
        else if (routine == 2)
        {
            mRoot = new BTRepeater(this,
                        new BTSelectorNode(this,
                            new BTNode[] { new BTFail(this), new BTRandomWalk(this) }));
        }
    }

    void Update()
    {
        if(!startedBehavior)
        {
            behavior = StartCoroutine(RunBehavior());
            startedBehavior = true;
        }
    }

    private IEnumerator RunBehavior()
    {
        BTNode.Result result = Root.Execute();

        while (result == BTNode.Result.Running)
        {
            Debug.Log("Root result: " + result);
            yield return null;
            result = Root.Execute();
        }

        Debug.Log("Behavior has finished with: " + result);
    }
}
