using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBall : MonoBehaviour
{
    public GameObject ballPrefab;

    protected GameObject createdBall;

    float spawnTime;
    
    public int functionMode;

    void LaunchProjectile()
    {
        createdBall = Instantiate<GameObject>(ballPrefab);
        Destroy(createdBall, 3f);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (functionMode == 2) { InvokeRepeating("LaunchProjectile", 2.0f, 3.0f); }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && functionMode == 1)
        {
            LaunchProjectile();
        }
        //if (Time.realtimeSinceStartup - spawnTime > 3)
        { 
          //  Destroy(createdBall);
            //createdBall = null;
        }
    }
}
