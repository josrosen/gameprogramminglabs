using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Physics : MonoBehaviour
{
    float xSpeed;
    float ySpeed;
    float gravity = 0.05f;
    Vector3 velocity;
    // Start is called before the first frame update
    void Start()
    {
        xSpeed = Random.Range(1f, 16f);
        ySpeed = Random.Range(1f, 12f);
        velocity = new Vector3(xSpeed, ySpeed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        velocity.y -= gravity;

        transform.Translate(velocity.x * Time.deltaTime, velocity.y * Time.deltaTime, 0);
    }
}
