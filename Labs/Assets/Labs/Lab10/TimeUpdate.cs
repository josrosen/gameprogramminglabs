using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeUpdate : MonoBehaviour
{
    // Start is called before the first frame update
    Text text;
    public SendMessageTimer timerObj;
    void Start()
    {
        text = gameObject.GetComponent<Text>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTimerTick()
    {
        text.text = "" + timerObj.TimeLeft();
    }

    //public void OnTimerTick(SendMessageTimer timer)
    //{
      //  text.text = "" + timer.TimeLeft();

    //}

    public void OnTimerExpired()
    {
        text.text = "0";
    }

}
