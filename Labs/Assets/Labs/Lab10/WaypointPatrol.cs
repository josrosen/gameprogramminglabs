using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPatrol : MonoBehaviour
{
    //speed to patrol at
    public float speed;
    //time to wait at each waypoint before moving on
    public float waitTime;

    //arraylist of points to travel to
    public List<Transform> waypoints;

    public bool runningRoutine;


    //a reference to the coroutine we create
    private Coroutine path;
    protected SpriteRenderer spr;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        runningRoutine = true;
        path = StartCoroutine(PatrolWaypoints());
    }

    //Stop the coroutine to begin a new set of events
    public void StopPatrolling()
    {
        StopCoroutine(path);
    }

    //Coroutine to walk through the waypoints
    protected IEnumerator PatrolWaypoints()
    {
        //patrol for forever, enless we externally kill the routine
        while(true)
        {
            foreach(Transform point in waypoints)
            {
                Debug.Log("pathing to " + point.position);
                //loop until we get to the next point
                while(transform.position != point.position)
                {
                    //set the position to be the result of moving from out
                    //position to the destination at the given speed
                    transform.position = Vector3.MoveTowards(transform.position, point.position, speed * Time.deltaTime);

                    spr.color = Color.Lerp(spr.color, point.GetComponent<SpriteRenderer>().color, speed * Time.deltaTime);
                    //relinquish control to the game inbetween movements,
                    //and we return null because we don't want to wait or do
                    //anything else between these movements
                    yield return null;
                }
                //we return a new coroutine designed to pause
                //for a specified time, which will then move control
                //back to us to move to the next waypoint
                yield return new WaitForSeconds(waitTime);
            }

            //continue the loop, starting over from the beginning
            yield return null;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            print("spacebar pressed");
            if(runningRoutine)
            {
                runningRoutine = false;
                StopPatrolling();
            }
            else
            {
                runningRoutine = true;
                path = StartCoroutine(PatrolWaypoints());
            }
        }
    }
}
