using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendMessageTimer : MonoBehaviour
{
    //how long the timer will last
    public float duration;

    //value to calculate the end time
    private float endTime;

    //bool to see if a time has sstarted
    private bool timerStarted;

    //game object to call messages on
    public GameObject calledObject;

    // Start is called before the first frame update
    void Start()
    {
        timerStarted = false;
        StartTimer();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public float TimeLeft()
    {
        if (Time.time >= endTime)
            return 0;
        else
            return endTime - Time.time;
                
    }

  

    public void StartTimer()
    {
        //
        timerStarted = true;
        StartCoroutine(StartCountdown());
    }

    protected virtual IEnumerator StartCountdown()
    {
        //calculate the end time based on the start time
        endTime = Time.time + duration;

        //loop until we pass the end time
        while(Time.time < endTime)
        {
            calledObject.SendMessage("OnTimerTick");
            yield return null;
        }

        //call this function, OnTimerExpired() on the game object
        calledObject.SendMessage("OnTimerExpired");
    }
}
