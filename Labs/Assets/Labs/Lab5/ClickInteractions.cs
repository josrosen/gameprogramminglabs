using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickInteractions : MonoBehaviour
{
    Vector3 mousePos;
    Vector3 worldPoint;
    Bounds bounds;

    Color defaultColor;

    SpriteRenderer spr; //attempt bounds with the sprite's extents
    //CircleCollider2D cirBound; //attempt bounds with a hitbox's extents (used just for testing)
    Camera myCamera;

    // Start is called before the first frame update
    void Start()
    {
        mousePos = Vector3.zero;
        spr = GetComponent<SpriteRenderer>();
        //cirBound = GetComponent<CircleCollider2D>();
        myCamera = Camera.main;
        worldPoint = Vector3.zero;
        

        if(spr == null) { Debug.LogError("No sprite renderer found"); }
        bounds = spr.bounds;
        defaultColor = spr.color;
        //if (cirBound == null) { Debug.LogError("No collider found"); }
        if (myCamera == null) { Debug.LogError("No camera found"); }
    }

    // Update is called once per frame
    void Update()
    {
        spr = GetComponent<SpriteRenderer>(); //If sprite moves from origin, we want to reverify every step
        bounds = spr.bounds;

        if(Input.GetKeyDown("mouse 0")) //We only want one frame of input, so make it the click down
        {
            mousePos = Input.mousePosition;
            worldPoint = myCamera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, mousePos.z));
            worldPoint.z = 0;
            print("The mouse has been clicked at x: " + mousePos.x + ", y: " + mousePos.y + ", z: " + mousePos.z);
            print("The global mouse pos is x: " + worldPoint.x + ", y: " + worldPoint.y + ", z: " + worldPoint.z);
            print("Bounds: " + bounds + "\nContains: " + bounds.Contains(worldPoint));

            if (bounds.Contains(worldPoint))
            {
                spr.color = Color.black;
            }
            else
            {
                spr.color = defaultColor;
            }
        }
    }
}
