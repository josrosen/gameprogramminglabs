using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    public float xSpeed;
    public float ySpeed;

    Vector3 movement;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        movement = Vector3.zero;
        //Up/Down/Left/Right movement
        if (Input.GetKey("up")) { movement.y += 1; }
        if (Input.GetKey("down")) { movement.y -= 1; }
        if (Input.GetKey("left")) { movement.x -= 1; }
        if (Input.GetKey("right")) { movement.x += 1; }

        //Adjust movement by our speed variables
        movement.x *= xSpeed;
        movement.y *= ySpeed;

        //execute movement
        transform.Translate(movement.x * Time.deltaTime, movement.y * Time.deltaTime, 0f);

    }
}
