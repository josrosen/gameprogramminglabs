using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjScript : MonoBehaviour
{
    public float initPos;
    public float endPos;
    private bool forward;
    private float movementSpeed;
    private float rotationSpeed;
    private Vector3 scaleAdjust;

    // Start is called before the first frame update
    void Start()
    {
        //Setting all variables as needed, including transferring values from our OperationScript
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        OperationScript sm = obj.GetComponent<OperationScript>();

        movementSpeed = sm.movementSpeed;
        rotationSpeed = sm.rotationSpeed;
        //Directly setting scalingSpeed does nothing useful, so let's make a vector instead
        scaleAdjust = new Vector3(sm.scalingSpeed, sm.scalingSpeed, 0);

        //I can't use coordinates for initPos/endPos so let's just use reference of the object's spawning area for points
        initPos = transform.position.x / 6;
        endPos = transform.position.x * 6;

        forward = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (forward)
        {
            //Switch directions once we reach endPos
            if (transform.position.x > endPos)
            {
                print("endPos reached, reversing direction");
                forward = false;
            }
            else
            {
                transform.Translate((movementSpeed * Time.deltaTime), 0.0f, 0.0f);
                transform.Rotate(rotationSpeed, 0, 0);
                transform.localScale += scaleAdjust;
                //transform.localScale.y += scalingSpeed;
            }
        }
        else
        {
            //Switch directions once we reach endPos
            if (transform.position.x < initPos)
            {
                print("initPos reached, reversing direction");
                forward = true;
            }
            else
            {
                transform.Translate((-movementSpeed * Time.deltaTime), 0.0f, 0.0f);
                transform.Rotate(-rotationSpeed, 0, 0);
                transform.localScale -= scaleAdjust;
                //transform.localScale.y -= scalingSpeed;
            }
        }

    }
}
