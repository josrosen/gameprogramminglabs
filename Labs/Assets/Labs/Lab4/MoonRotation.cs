using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonRotation : MonoBehaviour
{
    private GameObject target;
    
    public float angleSpeed;
    public string tagName;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag(tagName);

    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(target.transform.position, Vector3.forward, angleSpeed * Time.deltaTime);
        //transform.Translate((movementSpeed * Time.deltaTime), 0.0f, 0.0f);
    }
}
