using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//for serialization
using System;
using System.Runtime.Serialization; 
[Serializable]


public class SaveGame : ISerializable
{
    public int Health { get; set; }
    public int Exp { get; set; }
    public Vector3 playerPosition { get; set; }

    public SaveGame()
    {
    
    }

    public SaveGame(SerializationInfo info, StreamingContext context)
    {
        Health = info.GetInt32("health");
        Exp = info.GetInt32("exp");
        playerPosition = new Vector3(
            info.GetSingle("posx"),
            info.GetSingle("posy"),
            info.GetSingle("posz"));
    }

    //store data from model into the save file
    public void StoreData(GameModel model)
    {
        Health = model.player.GetComponent<Player>().health;
        Exp = model.player.GetComponent<Player>().experience;
        playerPosition = model.player.transform.position;
    }

    //load the data from the save file back into the model
    public void LoadData(GameModel model)
    {
        model.player.GetComponent<Player>().health = Health;
        model.player.GetComponent<Player>().experience = Exp;
        model.player.transform.position = playerPosition;
    }

    //called by the serialization method to write
    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("health", Health);
        info.AddValue("exp", Exp);
        info.AddValue("posx", playerPosition.x);
        info.AddValue("posy", playerPosition.y);
        info.AddValue("posz", playerPosition.z);
    }
}
