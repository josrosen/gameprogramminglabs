using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//for serialization
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class GameModel : MonoBehaviour
{
    public GameObject player;
    public string saveFilename;

    //private instance of a game model
    private static GameModel mSingleton;
    //this allows to access the instance wherever we are in code without
    //having to look up by tag, etc.
    public static GameModel Instance { get { return mSingleton; } }

    //Is called immediately after the constructor is
    void Awake()
    {
        //if the singleton hasn't been set, set it now
        if (mSingleton == null)
        {
            mSingleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SaveGameModel(SaveGame save, string filename)
    {
        //create a binary formatter to write objects to a stream
        BinaryFormatter bf = new BinaryFormatter();

        //create a file that we store data to
        FileStream fs = File.OpenWrite(Application.persistentDataPath + "/" + filename + ".dat");

        //from the save game object
        save.StoreData(this);

        //then we can serialize this
        bf.Serialize(fs, save);

        //close the filestream when we're done
        fs.Close();
    }

    public void LoadGame(string filename)
    {
        //we need a binary formatter to read
        BinaryFormatter bf = new BinaryFormatter();

        //open the file for reading
        FileStream fs = File.OpenRead(Application.persistentDataPath + "/" + filename + ".dat");

        //deserialize the save game, this will throw an exception if we can't
        SaveGame saveGame = (SaveGame)bf.Deserialize(fs);

        //we assume we have access to the game model
        saveGame.LoadData(this);

        //ALWAYS close
        fs.Close();
    }

    public void OnSaveClick()
    {
        SaveGame saveGame = new SaveGame();

        //then write it out
        SaveGameModel(saveGame, saveFilename);
    }

    public void OnLoadClick()
    {
        LoadGame(saveFilename);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
