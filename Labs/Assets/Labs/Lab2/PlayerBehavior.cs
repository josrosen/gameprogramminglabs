using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public float xSpeed;
    private Animator charAnimator;
    private SpriteRenderer spriteObj;
    GameManagement manager;


    void Start()
    {
        //Looks for a component of type Animator that's
        //attached to the gameObject that owns this script
        charAnimator = GetComponent<Animator>();
        spriteObj = GetComponent<SpriteRenderer>();

        if (charAnimator == null)
        {
            Debug.LogError("No animator attached");
        }
        if(spriteObj == null)
        {
            Debug.LogError("No spriterenderer attached");
        }

        manager = GameObject.FindWithTag("GameManager").GetComponent<GameManagement>();

        if (manager == null)
            Debug.LogError("No GameManager found");

        print("The Player name in start is " + gameObject.GetComponentInChildren<Text>().text);

    }

    // Update is called once per frame
    void Update()
    {
        print("The Player name in update is " + gameObject.GetComponentInChildren<Text>().text);

        if (gameObject.GetComponentInChildren<Text>().text != manager.getPlayerName())
            gameObject.GetComponentInChildren<Text>().text = manager.getPlayerName();

        print("The Player name in update is " + gameObject.GetComponentInChildren<Text>().text);

        float horizontalAxis = Input.GetAxis("xMov");

        //Set facing (could be a bool, but wanted to be flexible just in case)
        if (horizontalAxis != 0)
        {
            if (horizontalAxis > 0)
            {
                spriteObj.flipX = false;
            }
            else
            {
                spriteObj.flipX = true;
            }
        }

        //Horizontal movement (Only one direction currently)
        if (Mathf.Abs(horizontalAxis) > 0.0f || Input.GetButton("xMov"))    //This prevents us to going to idle animation when changing directions
        {
            print("Horizontal movement active");
            transform.Translate((xSpeed * horizontalAxis * Time.deltaTime), 0.0f, 0.0f);
            charAnimator.SetFloat("Horizontal Movement", xSpeed);
            charAnimator.SetBool("Standing Still", false);
            //hi
        }
        else
        {
            charAnimator.SetFloat("Horizontal Movement", 0.0f);
            charAnimator.SetBool("Standing Still", true);
        }
    }
}
