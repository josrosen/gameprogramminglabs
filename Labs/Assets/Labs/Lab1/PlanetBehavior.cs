using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public float xSpeed;
    public float rotationSpeed;

    //This is not to be modified from Unity's side
    private bool toggleRotation = true;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //All of this code was created with reference from the API (Lab1)

        //When it's a new tick, update our log
        print("New update in PlanetBehavior");
        
        //Horizontal movement (Only one direction currently)
        if (Input.GetButton("xMov"))    //I mapped xMov to left/right and A/D keys
        {
            print("Horizontal movement active");
            transform.Translate(xSpeed * Time.deltaTime, 0, 0);
        }

        //Toggle rotation (thought it would be a fun and simple additon)
        if (Input.GetKeyDown(KeyCode.Space))   
        {
            print("Rotation has been toggled");
            toggleRotation = toggleRotation ? false : true;
        }
        if(toggleRotation)
            transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }
}
