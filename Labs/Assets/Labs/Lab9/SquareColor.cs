using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareColor : MonoBehaviour
{
    GameManagement manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindWithTag("GameManager").GetComponent<GameManagement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<SpriteRenderer>().color != manager.getColor())
            gameObject.GetComponent<SpriteRenderer>().color = manager.getColor();
    }
}
