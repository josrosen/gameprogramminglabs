using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    private string playerName;
    private Color myColor;

    private int spawnCount = 0;
    public void setPlayerName(string name)
    {
        Debug.Log("player name set to: " + name);
        playerName = name;
    }

    public string getPlayerName()
    {
        return playerName;
    }

    public void setColor(Color theColor)
    {
        myColor = theColor;
    }

    public Color getColor()
    {
        return myColor;
    }
    // Start is called before the first frame update
    void Start()
    {
        //Defaults
        setPlayerName("Player");
        setColor(Color.black);

        spawnCount += 1;
        print("The game manager has spawned " + spawnCount + " time(s)");

        //keep our game manager between scenes
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        //print("Player name is " + getPlayerName() + " and the color used is " + getColor());
    }
}
