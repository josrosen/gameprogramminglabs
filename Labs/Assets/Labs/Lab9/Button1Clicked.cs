using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Button1Clicked : MonoBehaviour
{
    public GameManagement manager;
    public Canvas myCanvas;
    InputField myInp;
    ToggleGroup myToggle;
    // Start is called before the first frame update
    void Start()
    {
        myInp = myCanvas.GetComponentInChildren<InputField>();
        myToggle = myCanvas.GetComponentInChildren<ToggleGroup>();

        if (myInp == null)
            Debug.LogError("InputField does not exist");
        if (myToggle == null)
            Debug.LogError("ToggleGroup does not exist");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void runFunction()
    {
        print("The mouse has been lifted");
        if (myInp.text == "") { manager.setPlayerName("Player"); print("Player Name set to \'Player\'"); }
        else { manager.setPlayerName(myInp.text); print("Player Name set to \'" + myInp.text + "\'"); }

        manager.setColor(myToggle.GetFirstActiveToggle().colors.normalColor);

        SceneManager.LoadScene(1);
    }
}
