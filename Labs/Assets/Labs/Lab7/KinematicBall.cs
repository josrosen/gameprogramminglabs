using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicBall : MonoBehaviour
{
    Vector3 Velocity;
    public float ySpeed;

    // Start is called before the first frame update
    void Start()
    {
        Velocity = new Vector3(0, ySpeed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        transform.Translate(new Vector3 (Velocity.x * Time.deltaTime, Velocity.y * Time.deltaTime, 0));   
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Velocity.y *= -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Velocity.y *= -1;

    }
}