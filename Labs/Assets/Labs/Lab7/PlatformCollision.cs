using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name + " collided with " + this.name);
        
    }

    public void OnTriggerEnter2D(Collider2D trigger)
    {
        Debug.Log(trigger.gameObject.name + " triggered with " + this.name);
    }
}
