using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeMovement : MonoBehaviour
{

    public float xSpeed;
    public float ySpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = new Vector3(xSpeed, ySpeed, 0);
        transform.Translate(movement.x * Time.deltaTime, movement.y * Time.deltaTime, 0);
    }
}
