using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    //switches frames between normal and pressed
    public SpriteAnim[] frames;
    //which way the button is facing determines whether it need a dash/stomp
    public string orientation;
    //Our name is stored for reference in GameManagement, so on reload it will remain pressed
    public string buttonName;
    //If the button is pressed once, it's pressed for the rest of the game
    bool isPressed;

    SideScrollController player;
    GameManagement manager;
    AudioSource buttonPress;

    //collision management
    public BoxCollider2D trigger;
    public BoxCollider2D triggerBlock;

    // Start is called before the first frame update
    void Start()
    {
        isPressed = false;
        player = FindObjectOfType<SideScrollController>();
        manager = FindObjectOfType<GameManagement>();
        buttonPress = GetComponent<AudioSource>();

        //Set button to active if already activated
        if (manager.ContainsButton(buttonName))
        {
            isPressed = true;
            trigger.enabled = false;
            triggerBlock.enabled = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Trigger against button detected");
        if(collision.tag == "Player")
        {
            if(!isPressed)
            {
                if(player.dashing && (orientation == "left" || orientation == "right"))
                {
                    isPressed = true;
                    trigger.enabled = false;
                    triggerBlock.enabled = false;
                    buttonPress.Play();
                    manager.AddButton(buttonName);
                }
                else if (player.stomping && orientation == "up")
                {
                    isPressed = true;
                    trigger.enabled = false;
                    triggerBlock.enabled = false;
                    buttonPress.Play();
                    manager.AddButton(buttonName);
                }
                
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("Collision against button detected");
        if (collision.gameObject.tag == "Player")
        {
            
            if (!isPressed)
            {
                if (player.dashing && (orientation == "left" || orientation == "right"))
                {
                    isPressed = true;
                    manager.AddButton(buttonName);
                }
                else if (player.stomping && orientation == "up")
                {
                    isPressed = true;
                    manager.AddButton(buttonName);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!isPressed)
        {
            GetComponent<SpriteRenderer>().sprite = frames[0].Frame(0);
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = frames[1].Frame(0);
        }
    }
}
