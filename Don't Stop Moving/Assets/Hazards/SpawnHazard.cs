using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHazard : MonoBehaviour
{
    public GameObject hazardPrefab;
    protected GameObject curHazard;

    public float offset;
    public float delay;
    public bool selfDestruct;
    public float destructDelay;

    void Spawn()
    {
        curHazard = Instantiate<GameObject>(hazardPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.localRotation);
        if (selfDestruct) Destroy(curHazard, destructDelay);
    }

    // Start is called before the first frame update
    void Start()
    {
        
        InvokeRepeating("Spawn", offset, delay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
