using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateScr : MonoBehaviour
{
    //Developer needs to indicate which button the gate corresponds to
    public string buttonName;

    //The state of the gate has 3 forms:
    //0: closed
    //1: opening animation
    //2: open
    int openState;

    bool playSound;

    //Sprite states
    public SpriteAnim closed;
    public SpriteAnim openAnim;
    public SpriteAnim open;

    SpriteRenderer spr;
    GameManagement manager;
    BoxCollider2D hitBox;
    AudioSource riseSound;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManagement>();
        spr = GetComponent<SpriteRenderer>();
        hitBox = GetComponent<BoxCollider2D>();
        riseSound = GetComponent<AudioSource>();

        //Check if the gate should start opened or not
        if (manager.ContainsButton(buttonName))
            openState = 2;
        else
            openState = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(manager.ContainsButton(buttonName))
        {
            if (openState == 0)
                openState = 1;
            else if (openState == 1 && animTime >= 31)
                openState = 2;
        }

        if(openState == 0)
        {
            hitBox.enabled = true;
        }
        else if(openState == 2)
        {
            hitBox.enabled = false;
        }

        if (playSound == true && !riseSound.isPlaying)
        {
            riseSound.Play();
            riseSound.loop = true;
        }
        else if (playSound == false && riseSound.isPlaying)
        {
            riseSound.Stop();
        }

        UpdateAnimation();
    }

    //keep class-wide so we can reference in future calls
    SpriteAnim currentAnim;
    SpriteAnim lastAnim;
    float animTime = 0;
    public void UpdateAnimation()
    {
        if (openState == 0)
        {
            currentAnim = closed;
        }
        else if (openState == 1)
        {
            currentAnim = openAnim;
            playSound = true;
        }
        else
        {
            currentAnim = open;
            playSound = false;
        }        

        //Actually run our determined sprite
        animTime += currentAnim.animSpeed * Time.deltaTime;
        if (currentAnim != lastAnim)
        {
            animTime = 0;
        }
        //Set class-wide vars for reference in the next call
        spr.sprite = currentAnim.Frame(animTime);
        lastAnim = currentAnim;

    }
}
