using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatform : MonoBehaviour
{
    bool passthrough;
    
    SideScrollController player;

    public BoxCollider2D hitbox;

    public float playerHeight;

    // Start is called before the first frame update
    void Start()
    {
        passthrough = false;
        hitbox = GetComponent<BoxCollider2D>();
        player = FindObjectOfType<SideScrollController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Transform feet = player.GetComponentInChildren<Transform>();
            if(feet.position.y > transform.position.y)
            {
                passthrough = false;
                hitbox.enabled = true;
            }
            else
            {
                passthrough = true;
                hitbox.enabled = false;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Transform feet = player.GetComponentInChildren<Transform>();
            if ((passthrough && feet.position.y - playerHeight > transform.position.y) && !player.dashing && !player.stomping)
            {
                passthrough = false;
                hitbox.enabled = true;
            } 
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (passthrough)
        {
            hitbox.enabled = false;
        }
        else
            hitbox.enabled = true;
    }
}
