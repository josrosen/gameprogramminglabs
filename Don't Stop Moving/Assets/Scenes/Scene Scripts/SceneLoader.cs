using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string nextSceneName;

    //This spawn code is unique to ONE spawner in the next room, so the gameManager knows where to find it
    public string spawnCode;

    private GameManagement manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindObjectOfType<GameManagement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            manager.setSpawn(nextSceneName, spawnCode);
            SceneManager.LoadScene(nextSceneName);

        }
    }
}
