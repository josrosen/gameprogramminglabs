using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToCredits : MonoBehaviour
{
    GameManagement manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManagement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("z"))
        {
            Destroy(manager);
            SceneManager.LoadScene("Credits");
        }
    }
}
