using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMain : MonoBehaviour
{
    GameManagement manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManagement>();
        manager.setSpawn("MainMenu", "");
        SceneManager.LoadScene("MainMenu");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
