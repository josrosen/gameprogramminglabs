using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeGameOverAppear : MonoBehaviour
{
    
    DeathAnimation anim;
    public GameObject text;
    public GameObject continuePress;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        anim = FindObjectOfType<DeathAnimation>();
        timer = 0;
        text.SetActive(false);
        continuePress.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (anim.animationComplete && timer == 0)
        {
            timer = Time.realtimeSinceStartup;
        }
        else if (anim.animationComplete && Time.realtimeSinceStartup - timer >= 3)
        {
            text.SetActive(true);
        }

        //Queue our continue prompt after game over music ends (about 5 secs long)
        if (anim.animationComplete && Time.realtimeSinceStartup - timer >= 8)
        {
            continuePress.SetActive(true);
        }
    }
}
