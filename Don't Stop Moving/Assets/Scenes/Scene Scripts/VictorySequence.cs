using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictorySequence : MonoBehaviour
{
    GameManagement manager;
    public GameObject fragment;
    public TextMesh fragmentText;
    public GameObject continuePrompt;
    public GameObject scorePrompt;

    float timer;

    bool fragmentSpawned = false;
    bool scoreSpawned = false;
    bool continueSpawned = false;
    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManagement>();
        fragment.SetActive(false);
        fragmentText.gameObject.SetActive(false);
        continuePrompt.SetActive(false);
        scorePrompt.SetActive(false);
        timer = Time.realtimeSinceStartup;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.realtimeSinceStartup - timer > 9 && !fragmentSpawned)
        {
            fragment.SetActive(true);
            fragmentText.gameObject.SetActive(true);

            //Modify the fragmentText
            string text = ": " + manager.GhostItemCount() + "/10    +" + (manager.GhostItemCount() * 10000);
            fragmentText.text = text;            
            if(manager.GhostItemCount() == 10)
            {
                fragmentText.color = Color.yellow;
            }
            else if (manager.GhostItemCount() >= 7)
            {
                fragmentText.color = Color.green;
            }
            else if (manager.GhostItemCount() >= 4)
            {
                fragmentText.color = Color.red;
            }
            else if (manager.GhostItemCount() >= 1)
            {
                fragmentText.color = Color.white;
            }
            else
            {
                fragmentText.color = Color.gray;
            }

            fragmentSpawned = true;

            fragmentText.GetComponent<AudioSource>().Play();

            //Add score
            manager.score += manager.GhostItemCount() * 10000;
        }

        if(Time.realtimeSinceStartup - timer > 10.5 && !scoreSpawned)
        {
            scoreSpawned = true;
            scorePrompt.SetActive(true);
            scorePrompt.GetComponent<AudioSource>().Play();
        }

        if(Time.realtimeSinceStartup - timer > 12 && !continueSpawned)
        {
            continueSpawned = true;
            continuePrompt.SetActive(true);
            continuePrompt.GetComponent<AudioSource>().Play();
        }

    }
}
