using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BeginGame : MonoBehaviour
{
    GameManagement manager;
    public AudioSource playSound;
    public AudioSource titleMusic;

    bool keyPressed = false;
    float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManagement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("z") && !keyPressed)
        {
            
            keyPressed = true;
            timer = Time.realtimeSinceStartup;
            titleMusic.Stop();
            playSound.Play();
        }

        if (keyPressed && Time.realtimeSinceStartup - timer > 1)
        {
            manager.setSpawn("Start_Room", "PlayerSpawnStart");
            manager.StartBGM();
            SceneManager.LoadScene("Start_Room");            
        }
    }
}
