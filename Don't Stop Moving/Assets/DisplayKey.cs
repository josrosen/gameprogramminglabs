using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayKey : MonoBehaviour
{
    public bool displayNow;
    public string abilityNeeded;

    SpriteRenderer spr;
    GameManagement manager;
    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        manager = FindObjectOfType<GameManagement>();
        if (!displayNow)
            spr.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!displayNow)
        {
            switch (abilityNeeded)
            {
                case "dash":
                    {
                        if(manager.canDash)
                        {
                            displayNow = true;
                            spr.enabled = true;
                        }    
                        break;
                    }
                case "wallJump":
                    {
                        if (manager.canWallJump)
                        {
                            displayNow = true;
                            spr.enabled = true;
                        }
                        break;
                    }
                case "doubleJump":
                    {
                        if (manager.jumpCount > 1)
                        {
                            displayNow = true;
                            spr.enabled = true;
                        }
                        break;
                    }
                case "stomp":
                    {
                        if (manager.canStomp)
                        {
                            displayNow = true;
                            spr.enabled = true;
                        }
                        break;
                    }
            }
        }
    }
}
