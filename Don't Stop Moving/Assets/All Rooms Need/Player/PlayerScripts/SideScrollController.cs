using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideScrollController : CharacterController2D
{
    //Physics variables
    public float xSpeed = 7f;
    public float dashSpeed = 15f;
    public float stompSpeed = 25f;
    public float jumpSpeed = 16f;
    public int maxJumpCount = 1;
    public int maxDashCount = 1;
    public int curJumpCount;
    int curDashCount;
    public float terminalVelocity = 40f;
    public float gravity = 33f;
    public Vector3 velocity = Vector3.zero;

    //Collision/Player Maintenance
    public float skinWidth = .01f / 16f;
    public int facing;
    private bool grounded;

    //Injury maintenance
    private Coroutine invulnerable;
    public bool isHurt = false;
    public float recoil = 9f;
    bool recoiling = false;

    //Input control
    bool inputAllowed = true; //disable during events or using abilities like dash/wall jump
    bool wallJumping = false;
    public bool dashing = false;
    public bool stomping = false;
    bool stompSoundPlayed = false;
    bool isEvent = false;
    
    

    //Timer
    float curTime = 0;
    float dashTimer = 0;
    float injuryTimer = 0;

    //Unity Components
    private GameManagement manager;
    private SpriteRenderer spriteObj;

    //Variables for SpriteAnim use
    public SpriteAnim idle;
    public SpriteAnim walk;
    public SpriteAnim jump;
    public SpriteAnim fall;
    public SpriteAnim wallHug;
    public SpriteAnim dash;
    public SpriteAnim injured;

    //Sounds
    public AudioSource jumpSound;
    public AudioSource hurtSound;
    public AudioSource dashSound;
    public AudioSource stompSound;
    public AudioSource stompImpactSound;
    public AudioSource ceilingBumpSound;

    //Particles
    public ParticleSystem jumpDust;
    public ParticleSystem wallJumpDustL;
    public ParticleSystem wallJumpDustR;
    public ParticleSystem dashDustL;
    public ParticleSystem dashDustR;
    public ParticleSystem stompDust;

    public bool IsGrounded
    {
        get
        {
            return IsTouching(Vector3.down * skinWidth);
        }
    }

    //I'm not sure the use of this yet, but Jon encouraged me to hold on to it
    public bool WillTouchGround
    {
        get
        {
            if (velocity.y < 0 && !IsTouching(Vector3.up *skinWidth))
                return IsTouching(Vector3.down * velocity.y * Time.deltaTime);
            else
                return false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Hazard" && !isHurt)
        {
            //determine which direction the player flies
            if (transform.position.x < collision.transform.position.x) { facing = 1; }
            else { facing = 2; }

            isHurt = true;
            recoiling = true;
            manager.playerHealth -= 10;
            grounded = false;
            inputAllowed = false;
            dashing = false;
            stomping = false;
            wallJumping = false;
            velocity.y = 6;
            curTime = Time.realtimeSinceStartup;
            hurtSound.Play();
            invulnerable = StartCoroutine(Iframes());
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Hazard" && !isHurt)
        {
            //determine which direction the player flies
            if (transform.position.x < collision.transform.position.x) { facing = 1; }
            else { facing = 2; }

            isHurt = true;
            recoiling = true;
            manager.playerHealth -= 10;
            grounded = false;
            inputAllowed = false;
            dashing = false;
            stomping = false;
            wallJumping = false;
            velocity.y = 6;
            curTime = Time.realtimeSinceStartup;
            hurtSound.Play();
            invulnerable = StartCoroutine(Iframes());
        }
    }

    private void Respawn()
    {
        //when respawning we need to reset any player states that would carry over otherwise
        velocity = Vector3.zero;
        grounded = true;
        inputAllowed = true;
        dashing = false;
        wallJumping = false;
        isEvent = false;
        facing = 1;
        
        //transform.position = respawnPos.transform.position;
    }

    public void SpawnIn(Transform respawnPos)
    {
        transform.position = respawnPos.transform.position;
    }

    //Makes player invulnerable and flickers sprite
    protected IEnumerator Iframes()
    {
        //flicker sprite
        injuryTimer = Time.realtimeSinceStartup;
        int i = 0;
        float alphaVar = ((float)manager.playerHealth / 100) + .3f; 
        while (Time.realtimeSinceStartup - injuryTimer < 2.60f)
        {
            //alternate between alpha levels
            if(i % 2 == 0)
            {
                spriteObj.color = new Color(spriteObj.color.r, spriteObj.color.g, spriteObj.color.b, alphaVar);
            }
            else
            {
                spriteObj.color = new Color(spriteObj.color.r, spriteObj.color.g, spriteObj.color.b, 0);
            }
            i++;
            yield return new WaitForSeconds(0.05f);
        }
        isHurt = false;
        yield break;
    }

    // Start is called before the first frame update
    void Start()
    {
        facing = 1; //Starts facing right (var unused but I plan to implement in future)
        curDashCount = maxDashCount;
        grounded = true;

        spriteObj = GetComponent<SpriteRenderer>();
        manager = GameObject.FindObjectOfType<GameManagement>();
        maxJumpCount = manager.jumpCount;
        curJumpCount = maxJumpCount;

        if (spriteObj == null) { Debug.LogError("No spriterenderer attached"); }
        if(manager == null) { Debug.LogError("Game Manager wasn't found"); }
    }

    // Update is called once per frame
    void Update()
    {
        //Anti-Lurch (catch input if framerate is too low)
        if (Time.deltaTime > .1) { return; }

        //check manager for jump count
        if (maxJumpCount < manager.jumpCount)
            maxJumpCount = manager.jumpCount;

        Vector3 input = Vector3.zero;
        if (inputAllowed)
        {
            //Obtain x-movement from player
            if (Input.GetKey("left")) { input.x -= 1; facing = 2; }
            if (Input.GetKey("right")) { input.x += 1; facing = 1; }

            //adjust the velocity with gravity
            if (IsGrounded || WillTouchGround)
            {
                grounded = true;
                velocity.y = 0;
                curJumpCount = maxJumpCount;
                curDashCount = maxDashCount;
                if (Input.GetKeyDown("z")) 
                { 
                    velocity.y += jumpSpeed; 
                    curJumpCount -= 1; 
                    grounded = false;
                    jumpSound.pitch = 1f;
                    jumpSound.Play();
                    createParticles(jumpDust);
                }
            }
            else
            {
                if (grounded) //If the player walks off a ledge, they lose a jump
                {
                    grounded = false;
                    curJumpCount -= 1;
                }

                //Jump properties
                if (velocity.y != 0)
                {
                    //Early release or player touches the ceiling
                    if (velocity.y > 0 && (Input.GetKeyUp("z") || IsTouching(Vector3.up * skinWidth))) 
                    { 
                        velocity.y = 0;

                        //ceiling touch happens if the key is still down
                        if (Input.GetKey("z"))
                            ceilingBumpSound.Play();
                    }

                    //walljump/double jump
                    if (Input.GetKeyDown("z"))
                    {
                        //walljump
                        if (manager.canWallJump && ((IsTouching(Vector3.left * skinWidth) && facing == 2) || (IsTouching(Vector3.right * skinWidth) && facing == 1)))
                        {
                            inputAllowed = false;
                            wallJumping = true;
                            velocity.y = 0;
                            velocity.y += jumpSpeed;
                            curTime = Time.realtimeSinceStartup;
                            curJumpCount = maxJumpCount - 1;
                            curDashCount = maxDashCount;
                            jumpSound.pitch = 1.05f;
                            jumpSound.Play();
                            if (facing == 1)
                                createParticles(wallJumpDustR);
                            else
                                createParticles(wallJumpDustL);
                        }
                        //double jump
                        else if (curJumpCount > 0)
                        {
                            velocity.y = 0; //resets our velocity
                            velocity.y += jumpSpeed;
                            jumpSound.pitch = 1.15f;
                            jumpSound.Play();
                            createParticles(jumpDust);
                            curJumpCount -= 1;
                        }
                    }

                    //Stomping
                    if(manager.canStomp && Input.GetKey("down") && Input.GetKeyDown("x"))
                    {
                        inputAllowed = false;
                        stomping = true;
                        input.x = 0;
                        velocity.y = jumpSpeed;
                        
                    }
                }

                velocity.y -= (gravity * Time.deltaTime);
            }            

            //Dashing
            if(manager.canDash && Input.GetKeyDown("left shift") && Time.realtimeSinceStartup - dashTimer > .3 && curDashCount > 0)
            {
                if (!IsTouching(Vector3.right * skinWidth) && !IsTouching(Vector3.left * skinWidth))
                {
                    inputAllowed = false;
                    dashing = true;
                    grounded = false;
                    velocity.y = 0;
                    curTime = Time.realtimeSinceStartup;
                    curDashCount -= 1;
                    dashSound.Play();
                    if (facing == 1)
                        createParticles(dashDustR);
                    else
                        createParticles(dashDustL);
                }
            }
        }
        else if(isHurt && recoiling)
        {
            //Knockback animation            
            if (facing == 1) { input.x = -1; }
            else if (facing == 2) { input.x = 1; }

            velocity.y -= (gravity * Time.deltaTime);
            
            if ((Time.realtimeSinceStartup - curTime) >= .25)
            {
                inputAllowed = true;
                recoiling = false;
                invulnerable = StartCoroutine(Iframes());
            }
        }
        else if (wallJumping)
        {
            if (Time.realtimeSinceStartup - curTime < .25 && Input.GetKey("z"))
            {
                //input.x = (prevInput.x * -1); //This could work, but only if the player is actively pushing against the wall
                if (facing == 1) { input.x = -1; }
                else if (facing == 2) { input.x = 1; }

                //Terminate wall jump early if player lets go of jump or hits the ceiling
                if (Input.GetKeyUp("z") || IsTouching(Vector3.up * skinWidth)) 
                { 
                    velocity.y = 0;

                    if (facing == 1) { facing = 2; }
                    else if (facing == 2) { facing = 1; }

                    wallJumping = false;
                    inputAllowed = true;

                    //ceiling bump
                    if (IsTouching(Vector3.up * skinWidth))
                        ceilingBumpSound.Play();

                }

                velocity.y -= (gravity * Time.deltaTime);
            }
            else
            {
                //if it's frame-perfect with the end of the timer then I want to cancel it
                if (Input.GetKeyUp("z")) { velocity.y = 0; }

                if (facing == 1) { facing = 2; }
                else if (facing == 2) { facing = 1; }

                wallJumping = false;
                inputAllowed = true;
            }
        }
        else if (dashing)
        {
            if(Time.realtimeSinceStartup - curTime < .20)
            {
                if (facing == 1 && !IsTouching(Vector3.right * skinWidth)) 
                {
                    input.x = 1;
                }
                else if (facing == 2 && !IsTouching(Vector3.left * skinWidth)) 
                {
                    input.x = -1;
                }
                else if(IsTouching(Vector3.right * skinWidth) || IsTouching(Vector3.left * skinWidth))
                {
                    input.x = 0;
                    dashing = false;
                    inputAllowed = true;
                    dashTimer = Time.realtimeSinceStartup;
                }
            }
            else
            {
                dashing = false;
                inputAllowed = true;
                dashTimer = Time.realtimeSinceStartup;
            }
        }
        else if(stomping)
        {
            //cooldown before moving
            if ((grounded && Time.realtimeSinceStartup - curTime > .25) || IsTouching(Vector3.up * skinWidth))
            {
                stomping = false;
                inputAllowed = true;
                stompSoundPlayed = false;

                //ceiling bump
                if (IsTouching(Vector3.up * skinWidth))
                    ceilingBumpSound.Play();
            }
            //terminate the stomp when the ground is hit
            else if ((IsGrounded || WillTouchGround) && !grounded)
            {
                velocity.y = 0;
                grounded = true;
                curTime = Time.realtimeSinceStartup;
                FindObjectOfType<GenerateStomp>().CallStompEffect();
                stompImpactSound.Play();
                createParticles(stompDust);
                
                //correct the height so player isn't stuck in ground
                //(still happening regardless of value, must be something else?)
                //leaving here just in case it minimizes the error
                //transform.Translate(new Vector3(0f, 0.4f, 0f));
            }
            else
            {
                if (velocity.y > 0 && !IsTouching(Vector3.up * skinWidth))
                {
                    velocity.y -= terminalVelocity * Time.deltaTime;
                }
                else
                {
                    velocity.y = -stompSpeed;
                    if (!stompSoundPlayed)
                    {
                        stompSound.Play();
                        stompSoundPlayed = true;
                    }
                }
            }
            
        }
        else if (isEvent)
        {

        }
        //else inputAllowed = true; //failsafe in case I mess something up

        //restrict our fall speed at a certain point
        if(velocity.y > terminalVelocity) { velocity.y = terminalVelocity; }
        if (velocity.y < -terminalVelocity) { velocity.y = -terminalVelocity; }

        //Calculate our final translation action
        Vector3 movement = Vector3.zero;

        if (dashing) { movement += input * dashSpeed; }
        else if (recoiling) { movement += input * recoil; }
        else { movement += input * xSpeed; }
        movement += velocity;
        Move(movement * Time.deltaTime);

        UpdateAnimation(input);
    }

    //keep class-wide so we can reference in future calls
    SpriteAnim currentAnim;
    SpriteAnim lastAnim;
    float animTime = 0;
    public void UpdateAnimation(Vector3 input)
    {
        //injury takes priority
        if (isHurt && recoiling)
        {
            currentAnim = injured;
        }
        else
        {
            //Which way is our player facing?
            if (input.x < 0) { spriteObj.flipX = true;}
            if (input.x > 0) { spriteObj.flipX = false; }

            //Determine if still or walking
            if (Mathf.Abs(input.x) > 0)
            {
                currentAnim = walk;
            }
            else
            {
                currentAnim = idle;
            }

            //if in the air, replace with jump/fall anims
            if (!IsGrounded && !WillTouchGround)
            {
                if (IsTouching(Vector3.left * skinWidth) && facing == 2 && !stomping && manager.canWallJump)
                {
                    currentAnim = wallHug;
                }
                else if (IsTouching(Vector3.right * skinWidth) && facing == 1 && !stomping && manager.canWallJump)
                {
                    currentAnim = wallHug;
                }
                else { currentAnim = velocity.y > 0 ? jump : fall; }
            }

            //if it's a dash, we override our other options
            if (dashing) { currentAnim = dash; }
        }

        //Actually run our determined sprite
        animTime += currentAnim.animSpeed * Time.deltaTime;
        if(currentAnim != lastAnim)
        {
            animTime = 0;
        }
        //Set class-wide vars for reference in the next call
        spriteObj.sprite = currentAnim.Frame(animTime);
        lastAnim = currentAnim;

        //Change player transparency based on damage
        if(!isHurt)
            spriteObj.color = new Color(spriteObj.color.r, spriteObj.color.g, spriteObj.color.b, ((float)manager.playerHealth / 100) + 0.3f);
    } 

    void createParticles(ParticleSystem particles)
    {
        particles.Play();
    }

}
