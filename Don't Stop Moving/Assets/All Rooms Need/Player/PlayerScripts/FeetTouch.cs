using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetTouch : SideScrollController
{
    protected bool feetTouching = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ground")
        {
            feetTouching = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            feetTouching = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Ground")
        {
            feetTouching = false;
        }
    }

}
