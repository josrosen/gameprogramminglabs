using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAnimation : MonoBehaviour
{
    SpriteRenderer spr;
    //Frame Sets
    public SpriteAnim stunned;
    public SpriteAnim morph1;
    public SpriteAnim morph2;
    public SpriteAnim ghost;

    //Timer
    float stunnedDuration;
    float morph1Duration;
    float morph2Duration;

    public bool animationComplete = false;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        stunnedDuration = Time.realtimeSinceStartup;
        morph1Duration = stunnedDuration;
        morph2Duration = stunnedDuration;
    }

    SpriteAnim currentAnim;
    SpriteAnim lastAnim;
    float animTime = 0;
    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup - stunnedDuration < 2)
            currentAnim = stunned;
        else if (Time.realtimeSinceStartup - morph1Duration < 2.7)
            currentAnim = morph1;
        else if (Time.realtimeSinceStartup - morph2Duration < 3.4)
            currentAnim = morph2;
        else
        { 
            currentAnim = ghost;

            animationComplete = true;
        }

        //Actually run our determined sprite
        animTime += currentAnim.animSpeed * Time.deltaTime;
        if (currentAnim != lastAnim)
        {
            animTime = 0;
        }

        //Set class-wide vars for reference in the next call
        spr.sprite = currentAnim.Frame(animTime);
        lastAnim = currentAnim;
    }
}
