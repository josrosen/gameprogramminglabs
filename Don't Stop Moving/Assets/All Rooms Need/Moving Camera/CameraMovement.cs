using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement: MonoBehaviour
{
    /*Camera control referenced by 
     * https://answers.unity.com/questions/878913/how-to-get-camera-to-follow-player-2d.html
     * https://answers.unity.com/questions/1006959/stop-camera-when-wall-hit-2d-game.html
    
     */
    public Transform player;
    GameManagement manager;
    public Vector3 offset;

    //Collision (We have a collider on the outside of the tileset, why not use this to stop the camera?)
    public Transform leftWall;
    public Transform rightWall;
    public Transform topCeiling;
    public Transform bottomFloor;

    //Camera's hitboxes (if they surpass the collisions at any given point, we need to fix immediately)
    public Transform leftEdge;
    public Transform rightEdge;
    public Transform topEdge;
    public Transform bottomEdge;


    //Rules for our camera
    float Height;
    float Zoom;
    float xMax;
    float xMin;
    float yMax;
    float yMin;

    //bools
    public bool xLock;
    public bool yLock;
    bool xMovable = true;
    bool yMovable = true;

    bool touchingLeft = false;
    bool touchingRight = false;
    bool touchingBottom = false;
    bool touchingTop = false;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManagement>();
        xMin = leftWall.position.x + 2f;
        xMax = rightWall.position.x - 2f;
        yMin = bottomFloor.position.y + 2f;
        yMax = topCeiling.position.y - 2f;
        Zoom = offset.z;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 transformComponents = Vector3.zero;
        transformComponents.z = Zoom;

        //x-direction transformations
        if (!xLock)
        {
            if (xMovable && (player.position.x > gameObject.transform.position.x + offset.x || player.position.x < gameObject.transform.position.x - offset.x))
            {
                transformComponents.x = player.position.x + offset.x;
                //transform.position = new Vector3(player.position.x + offset.x, offset.y, offset.z);
            }
            else if (touchingLeft && (player.position.x) > gameObject.transform.position.x + offset.x)
            {
                xMovable = true;
                touchingLeft = false;
            }
            else if (touchingRight && (player.position.x) < gameObject.transform.position.x - offset.x)
            {
                xMovable = true;
                touchingRight = false;
            }
        }

        //y-direction transformations
        if (!yLock)
        {
            if (yMovable && (player.position.y > gameObject.transform.position.y + offset.y || player.position.y < gameObject.transform.position.y - offset.y))
            {
                transformComponents.y = player.position.y + offset.y;
                //transform.position = new Vector3(player.position.x + offset.x, offset.y, offset.z);
            }
            else if (touchingBottom && (player.position.y) > gameObject.transform.position.y + offset.y)
            {
                yMovable = true;
                touchingBottom = false;
            }
            else if (touchingTop && (player.position.y) < gameObject.transform.position.y - offset.y)
            {
                yMovable = true;
                touchingTop = false;
            }
        }
        if (transformComponents.x == 0)
            transformComponents.x = transform.position.x;
        if (transformComponents.y == 0)
            transformComponents.y = transform.position.y;

        //Check for exceeding bounds (expected during snap to player's spawn-in, so only done when changingRooms = true)
        if (manager.changingRooms)
        {
            //No snaps if the axis is locked
            if (!xLock)
            {
                if (leftEdge.transform.position.x < xMin - 2f)
                {
                    print("Camera is out of leftmost bounds of bounds... Corrected");
                    transformComponents.x = (leftWall.position.x - (leftEdge.position.x - transform.position.x));
                    touchingLeft = true;
                    xMovable = false;
                }

                if (rightEdge.transform.position.x > xMax + 2f)
                {
                    print("Camera is out of rightmost bounds... Corrected");
                    xMovable = false;
                    transformComponents.x = transformComponents.x = (rightWall.position.x - (rightEdge.position.x - transform.position.x));
                    touchingRight = true;
                }
            }

            //No snaps if the axis is locked
            if (!yLock)
            {
                if (topEdge.transform.position.y > yMax + 2f)
                {
                    print("Camera is out of upper bounds... Corrected");

                    transformComponents.y = (topCeiling.position.y - (topEdge.position.y - transform.position.y));
                    touchingTop = true;
                    yMovable = false;
                }

                if (bottomEdge.transform.position.y < yMin - 2f)
                {
                    print("Camera is out of lower bounds... Corrected ");
                    transformComponents.y = (bottomFloor.position.y - (bottomEdge.position.y - transform.position.y));
                    touchingBottom = true;
                    yMovable = false;
                }
            }

        }

        //shift our camera based on the given information
        transform.position = transformComponents;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "CameraCollider")
        {
            //check for x
            if (collision.transform.position.x == xMin - 2f)
            {
                xMovable = false;
                touchingLeft = true;
                print("Camera is touching left wall");
            }
            else if (collision.transform.position.x == xMax + 2f)
            {
                xMovable = false;
                touchingRight = true;
                print("Camera is touching right wall");
            }

            //check for y
            if(collision.transform.position.y == yMin - 2f)
            {
                yMovable = false;
                touchingBottom = true;
                print("Camera is touching bottom floor");
            }
            else if(collision.transform.position.y == yMax + 2f)
            {
                yMovable = false;
                touchingTop = true;
                print("Camera is touching top ceiling");
            }

        }
    }
}
