using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateStomp : MonoBehaviour
{
    float shakeSpeed = 0.1f;

    Coroutine screenShake;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CallStompEffect()
    {
        screenShake = StartCoroutine(StompEffect());
    }

    public IEnumerator StompEffect()
    {
        int i = 1;
        for(float timer = Time.realtimeSinceStartup; Time.realtimeSinceStartup - timer < .24 ; i++)
        {
            int modifier;
            if (i % 2 == 0)
                modifier = 1;
            else
                modifier = -1;

            transform.Translate(new Vector3(shakeSpeed * modifier, 0f, 0f));
            yield return null; 
        } 
    }
}
