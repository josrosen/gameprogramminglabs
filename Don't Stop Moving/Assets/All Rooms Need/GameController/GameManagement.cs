using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Used to keep track of how our sprites are animated (used to be in player script but moved to use with other objs)
[System.Serializable]
public class SpriteAnim
{
    public Sprite[] poses;
    public float animSpeed = 12;
    public Sprite Frame(float time)
    {
        if (poses == null || poses.Length == 0) { return null; }
        int frame = ((int)time) % poses.Length;
        return poses[frame];
    }

}

public class GameManagement : MonoBehaviour
{
    public int score;

    //Player variables
    public int playerHealth;
    public int maxPlayerHealth = 100;
    protected bool canRespawn;

    //Player Abilities
    public bool canDash = false;
    public bool canWallJump = false;
    public bool canStomp = false;
    public int jumpCount;

    //Spawning 
    private string spawnCode;
    private string currentRoom;
    private string newRoom;

    //Stored objects we want to modify on scene loading
    public List<string> activatedButtons;
    protected List<string> collectedItems;
    public List<string> collectedGhostItems;
    public bool changingRooms;
    float changeTimer;

    protected GameObject[] spawnPoints;
    public Transform decidedSpawn;
    protected SideScrollController existingPlayer;
    protected GhostBehavior existingGhost;
    private AudioSource bgm;
    
    //Spawning the player and ghost in new rooms
    protected void findPlayer()
    {
        existingPlayer = FindObjectOfType<SideScrollController>(); ;
    }

    protected void findGhost()
    {
        existingGhost = FindObjectOfType<GhostBehavior>();
    }

    protected void findSpawns()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawner");
    }

    protected Transform getSpawn(string key)
    {
        for(int i = 0; i < spawnPoints.Length; i++)
        {
            if (spawnPoints[i].name == key)
                return spawnPoints[i].transform;
        }

        return spawnPoints[0].transform;  //failsafe, just to be sure
    }
  
    public void setSpawn(string roomName, string code)
    {
        spawnCode = code;
        newRoom = roomName;
    }

    private Vector3 ghostSpawn()
    {
        Vector3 thePos = existingPlayer.transform.position;
        switch(spawnCode)
        {
            case "PlayerSpawnLeft":
            case "PlayerSpawnMiddleLeft":
            case "PlayerSpawnUpperLeft":            
                {
                    thePos.x -= 10;
                    break;
                }
            case "PlayerSpawnRight":
            case "PlayerSpawnMiddleRight":
            case "PlayerSpawnUpperRight":
                {
                    thePos.x += 10;
                    break;
                }
            case "PlayerSpawnTop":
            case "PlayerSpawnTopRight":
            case "PlayerSpawnTopLeft":
                {
                    thePos.y += 10;
                    break;
                }
            case "PlayerSpawnBottom":
            case "PlayerSpawnBottomRight":
            case "PlayerSpawnBottomLeft":
                {
                    thePos.y -= 10;
                    break;
                }
            case "PlayerSpawnStart":
                {
                    return existingGhost.transform.position;
                }

            default: return existingGhost.transform.position;
        }

        return thePos;
    }

    private void determineSpawn()
    {
        currentRoom = newRoom;
        findSpawns();
        decidedSpawn = getSpawn(spawnCode);
        findPlayer();
        existingPlayer.transform.position = decidedSpawn.position;
        findGhost();
        existingGhost.transform.position = ghostSpawn();

    }

    //Add a button to our active buttons
    public void AddButton(string b)
    {
        activatedButtons.Add(b);
    }

    public bool ContainsButton(string b)
    {
        if (activatedButtons.Count == 0)
            return false;

        foreach(string s in activatedButtons)
        {
            if (b == s)
                return true;
        }

        return false;
    }

    public void AddItem(string i)
    {
        collectedItems.Add(i);
    }

    public bool ContainsItem(string i)
    {
        if (collectedItems.Count == 0)
            return false;

        foreach(string s in collectedItems)
        {
            if (i == s)
                return true;
        }

        return false;
    }

    public void AddGhostItem(string i)
    {
        collectedGhostItems.Add(i);
    }

    public bool ContainsGhostItem(string i)
    {
        if (collectedGhostItems.Count == 0)
            return false;

        foreach (string s in collectedGhostItems)
        {
            if (i == s)
                return true;
        }

        return false;
    }

    public int GhostItemCount()
    {
        return collectedGhostItems.Count;
    }


    public void StartBGM()
    {
        bgm.Play();
        bgm.loop = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        jumpCount = 1;
        playerHealth = maxPlayerHealth;
        newRoom = SceneManager.GetActiveScene().name;
        activatedButtons = new List<string>();
        collectedItems = new List<string>();
        collectedGhostItems = new List<string>();
        changingRooms = true;
        changeTimer = Time.realtimeSinceStartup;
        bgm = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        //deactivate room changing bool after 1 sec
        if (changingRooms && Time.realtimeSinceStartup - changeTimer > 1)
            changingRooms = false;

        //commence death animation
        if (playerHealth <= 0)
        {
            //prevent from happening more than once
            playerHealth = 1;
            newRoom = "DeathRoom";
            bgm.Stop();
            SceneManager.LoadScene("DeathRoom");
        }
        
        //expected to fail when the room is initially loading, but still will work afterwards
        string activeScene = SceneManager.GetActiveScene().name;

        //Commence room change
        if (newRoom != activeScene)
        {
            changeTimer = Time.realtimeSinceStartup;
            changingRooms = true;
            if (newRoom == "VictoryRoom")
                bgm.Stop();
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(newRoom));
        }

        if (currentRoom != activeScene)
            determineSpawn();

    }
}
