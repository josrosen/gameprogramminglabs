using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GhostBehavior : MonoBehaviour
{
    //The Target to follow (our player)
    SideScrollController player;
    
    //Other reference objects
    GameManagement manager;
    AudioSource hurtSound;
    public ParticleSystem damageEffect;

    //Damage variables
    private Coroutine attack;
    public float attackSpeed = 0.5f;

    //Movement factors
    public float xSpeed;
    public float yAccel;
    Vector3 velocity;
    Vector3 direction;
    bool touchingPlayer;

    //Limiters for the ghost (I mainly had these for reference to our modification breakup, most aren't checked in code)
    float xMax = 5;
    float yMax = 2; //max is 5
    float attackMax = 0.1f;
    float scaleMax = 3;

    //Modifications to ghost power
    float addAttack = 0;
    float addxSpeed = 0;
    float addyLimit = 0;
    float addScale = 0;

    //Since the ghost is recreated each room, we need to add back the modifiers each time
    int itemsGrabbed = 0;
    bool dashModAdded = false;
    bool wallJumpModAdded = false;
    bool doubleJumpModAdded = false;
    bool stompModAdded = false;

    //For animations
    public SpriteAnim walk;
    private SpriteRenderer spriteObj;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if it's the player, then the ghost will respawn 
        if (collision.gameObject.tag == "Player")
        {
            touchingPlayer = true;
            damageEffect.Play();
            attack = StartCoroutine(DamagePlayer());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            touchingPlayer = false;
            damageEffect.Stop();
            stopAttack();
        }
    }

    protected IEnumerator DamagePlayer()
    {
        hurtSound.Play();
        while(touchingPlayer)
        {
            manager.playerHealth -= 1;
            yield return new WaitForSeconds(attackSpeed);
        }
        yield return null;
    }

    private void stopAttack()
    {
        StopCoroutine(attack);
        hurtSound.Stop();
    }

    // Start is called before the first frame update
    void Start()
    {
        spriteObj = GetComponent<SpriteRenderer>();
        player = FindObjectOfType<SideScrollController>();
        manager = FindObjectOfType<GameManagement>();
        hurtSound = GetComponent<AudioSource>();
        touchingPlayer = false;
        velocity = Vector3.zero;
        direction = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        //Calculate for modifier adjustments
        itemsGrabbed = manager.GhostItemCount();
        addxSpeed = 0;
        addyLimit = 0;
        //Each powerup adds to the ghost's power
        if (manager.canDash)
            addxSpeed += 1;
        if (manager.canWallJump)
            addyLimit += 0.5f;
        if (manager.jumpCount > 1)
            addyLimit += 0.5f;
        if (manager.canStomp)
            addxSpeed += 0.4f;

        //Scale and attack speed change relatively, and only apply to obtained items
        addxSpeed += (float)(0.16 * itemsGrabbed);
        addyLimit += (float)(0.1 * itemsGrabbed);
        addScale = (float)(0.2 * itemsGrabbed);
        addAttack = (float)(0.04 * itemsGrabbed);

        //Actually adjust the stats
        xSpeed = 2 + addxSpeed;
        yMax = 2 + addyLimit;
        attackSpeed = 0.5f - addAttack;
        //I chose to modify the scale in our UpdateAnimation function

        //Find where player is in the world, if they are
        Vector3 movement = Vector3.zero;
        //If Exactly equal, don't continue to switch directions
        if(player != null)
        {
            //X direction
            if (transform.position.x == player.transform.position.x) { direction.x = 0; }
            else if (transform.position.x < player.transform.position.x) { direction.x = 1; }
            else { direction.x = -1; }

            //Y direction
            if (transform.position.y == player.transform.position.y) { direction.y = 0; }
            else if (transform.position.y < player.transform.position.y) { direction.y = 1; }
            else { direction.y = -1; }

            if (velocity.y < yMax && direction.y > 0) { velocity.y += yAccel; }
            else if (velocity.y > -yMax && direction.y < 0) { velocity.y -= yAccel; }

            //failsafe modifications

            movement = new Vector3(direction.x * xSpeed, velocity.y, 0);

            transform.Translate(movement.x * Time.deltaTime, movement.y * Time.deltaTime, 0);
        }

        UpdateAnimation(movement);
    }

    //keep class-wide so we can reference in future calls
    SpriteAnim currentAnim;
    SpriteAnim lastAnim;
    float animTime = 0;
    public void UpdateAnimation(Vector3 input)
    {
        //Set the size of the ghost (and the particles)
        transform.localScale = new Vector3(1 + addScale, 1 + addScale, 0);
        damageEffect.transform.localScale = new Vector3(1 + addScale, 1 + addScale, 0);
        currentAnim = walk;

        if (!touchingPlayer && player)
        { 
            if (input.x > 1) { spriteObj.flipX = true; }
            else if (input.x < 1) { spriteObj.flipX = false; }
        }
 
        //Actually run our determined sprite
        animTime += currentAnim.animSpeed * Time.deltaTime;
        if (currentAnim != lastAnim)
        {
            animTime = 0;
        }
        //Set class-wide vars for reference in the next call
        spriteObj.sprite = currentAnim.Frame(animTime);
        lastAnim = currentAnim;

    }
}
