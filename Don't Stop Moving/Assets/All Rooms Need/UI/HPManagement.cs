using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPManagement : MonoBehaviour
{
    GameManagement manager;
    
    int hp;

    // Start is called before the first frame update
    void Start()
    {
        hp = 55;
        manager = FindObjectOfType<GameManagement>();
    }

    // Update is called once per frame
    void Update()
    {
        hp = manager.playerHealth;
        gameObject.GetComponent<TextMesh>().text = hp.ToString();

        if (hp < 20) { gameObject.GetComponent<TextMesh>().color = Color.red; }
        else { gameObject.GetComponent<TextMesh>().color = Color.white; }
    }
}
