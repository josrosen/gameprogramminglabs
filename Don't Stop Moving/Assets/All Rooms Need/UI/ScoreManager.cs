using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    GameManagement manager;

    int score;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        manager = FindObjectOfType<GameManagement>();
    }

    public void AddScore(int amount)
    {
        score = amount;

        gameObject.GetComponent<TextMesh>().text = "Score: " + ScoreString(amount.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if (score != manager.score)
            AddScore(manager.score);
    }

    private string ScoreString(string scoreStr)
    {

        while(scoreStr.Length < 7)
        {
            scoreStr = string.Concat("0", scoreStr);
        }

        return scoreStr;
    }
}
