using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGet : MonoBehaviour
{
    //Sets item apart from others so it spawns off
    string uniqueID;

    //How much score to add for obtaining
    public int scoreValue;
    //How much healing to give player
    public int restoreValue;

    //To make this more universal for obtainables, we can
    //split by the point of grabbing. Current purposes:
    //Score: simply there for a score modifier
    //Healing: restores player HP
    //Upgrade: gives the player additional abilities
    public string purpose;

    public ParticleSystem itemParticles;

    bool isCollected = false;
    float timer;

    GameManagement manager;
    SideScrollController player;
    AudioSource sound;


    // Start is called before the first frame update
    void Start()
    {
        uniqueID = name;

        manager = FindObjectOfType<GameManagement>();
        player = FindObjectOfType<SideScrollController>();
        sound = GetComponent<AudioSource>();

        if(manager.ContainsItem(uniqueID))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && !isCollected)
        {
            if (purpose == "Score")
            {

            }
            else if (purpose == "Healing")
            {
                manager.playerHealth += restoreValue;
                if (manager.playerHealth > manager.maxPlayerHealth)
                    manager.playerHealth = manager.maxPlayerHealth;
            }
            else if (purpose == "Upgrade")
            {
                if (tag == "DoubleUp")
                {
                    manager.jumpCount = 2;
                }
                else if (tag == "WallUp")
                {
                    manager.canWallJump = true;
                }
                else if (tag == "DashUp")
                {
                    manager.canDash = true;
                }
                else if (tag == "StompUp")
                {
                    manager.canStomp = true;
                }   
            }
            else if (purpose == "Ghost")
            {
                manager.AddGhostItem(uniqueID);
            }

            sound.Play();
            if(itemParticles != null) itemParticles.Play();
            isCollected = true;
            timer = Time.realtimeSinceStartup;
            manager.AddItem(uniqueID);
            manager.score += scoreValue;
            gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isCollected && Time.realtimeSinceStartup - timer > 1)
        {
            Destroy(gameObject);
        }
    }
}
